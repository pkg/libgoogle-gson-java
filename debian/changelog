libgoogle-gson-java (2.10-1+apertis1) apertis; urgency=medium

  * Move package to development repository. Needed for the Java suite

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Tue, 10 Oct 2023 21:46:48 +0530

libgoogle-gson-java (2.10-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 05 Oct 2023 17:57:44 +0000

libgoogle-gson-java (2.10-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.10
  * Set Rules-Requires-Root: no in debian/control

 -- tony mancill <tmancill@debian.org>  Wed, 16 Nov 2022 20:28:04 -0800

libgoogle-gson-java (2.9.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.9.1
  * Remove codegen from list of poms (removed upstream)

 -- tony mancill <tmancill@debian.org>  Sun, 07 Aug 2022 13:06:39 -0700

libgoogle-gson-java (2.9.0-1) unstable; urgency=high

  * Team upload.
  * New upstream version 2.9.0
    Addresses CVE-2022-25647 (Closes: #1010670)
  * Bump Standards-Version to 4.6.1.0
  * Ignore org.moditect:moditect-maven-plugin
  * Mark libgoogle-gson-java Multi-Arch: foreign
  * Freshen years in debian/copyright

 -- tony mancill <tmancill@debian.org>  Wed, 11 May 2022 20:54:07 -0700

libgoogle-gson-java (2.8.8-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version updated to 4.6.0.1
  * Switch to debhelper level 13

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 20 Sep 2021 01:09:03 +0200

libgoogle-gson-java (2.8.6-1+deb11u1+apertis0) apertis; urgency=medium

  * Sync from debian/bullseye-security.

 -- Apertis CI <devel@lists.apertis.org>  Sun, 18 Sep 2022 14:05:21 +0000

libgoogle-gson-java (2.8.6-1+apertis1) apertis; urgency=medium

  * Set component to sdk. Move java packages to sdk to avoid building
    for arm architecture.

 -- Vignesh Raman <vignesh.raman@collabora.com>  Tue, 22 Feb 2022 17:22:25 +0530

libgoogle-gson-java (2.8.6-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 15:31:12 +0000

libgoogle-gson-java (2.8.6-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version updated to 4.5.0
  * Switch to debhelper level 12

 -- Emmanuel Bourg <ebourg@apache.org>  Sun, 31 May 2020 22:32:55 +0200

libgoogle-gson-java (2.8.5-3co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 20 Feb 2021 01:01:24 +0000

libgoogle-gson-java (2.8.5-3) unstable; urgency=medium

  * Team upload.
  * Remove the OpenIDE-Module entry again but sync the Manifest file with all
    the information from the previous 2.7.0 version.

 -- Markus Koschany <apo@debian.org>  Fri, 25 Jan 2019 11:54:04 +0100

libgoogle-gson-java (2.8.5-2) unstable; urgency=medium

  * Team upload.
  * Add OpenIDE-Module entry to MANIFEST file to fix an error in Netbeans 10.
  * Declare compliance with Debian Policy 4.3.0.

 -- Markus Koschany <apo@debian.org>  Sun, 13 Jan 2019 23:39:43 +0100

libgoogle-gson-java (2.8.5-1) unstable; urgency=medium

  * New upstream release
    - Removed the Java 9 patches (fixed upstream)
    - Replaced templating-maven-plugin with sed
  * Ignore the broken tests to work around the build failure with Java 11
    (Closes: #912433)
  * Restored the parent of gson/pom.xml to inherit the build settings
  * Updated the watch file for the recent releases
  * Standards-Version updated to 4.2.1
  * Exclude the Gradle wrapper from the upstream tarball
  * Use salsa.debian.org Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 08 Nov 2018 12:04:52 +0100

libgoogle-gson-java (2.8.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.8.2.
    - Fix FTBFS with Java 9. (Closes: #893238)
  * Drop maven.cleanIgnoreRules and maven.publishedRules because they do
    nothing.
  * Use compat level 11.
  * Declare compliance with Debian Policy 4.1.3.
  * Install the MANIFEST file with javahelper since the bnd-maven-plugin is not
    available.

 -- Markus Koschany <apo@debian.org>  Sun, 08 Apr 2018 19:51:28 +0200

libgoogle-gson-java (2.4-2) unstable; urgency=medium

  * Team upload.
  * Build with the DH sequencer instead of CDBS
  * Added the missing build dependency on junit4
  * Standards-Version updated to 4.1.0
  * Switch to debhelper level 10

 -- Emmanuel Bourg <ebourg@apache.org>  Sat, 16 Sep 2017 22:20:53 +0200

libgoogle-gson-java (2.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Adapted the build to the new project layout
  * debian/control:
    - Standards-Version updated to 3.9.6 (no changes)
    - Updated the Homepage field
  * debian/watch: Watch the release tags on Github
  * Switch to debhelper level 9

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 03 Nov 2015 22:50:32 +0100

libgoogle-gson-java (2.2.4-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Use canonical URLs for the Vcs-* fields
    - Updated Standards-Version to 3.9.4 (no changes)
    - Removed the build dependency on libmaven-cobertura-plugin-java
    - Removed the build dependency on libmaven-assembly-plugin-java
    - Removed the explicit build dependency on libmaven-compiler-plugin-java
    - Removed the explicit build dependency on maven-repo-helper
    - Removed the unused build dependency on javahelper
  * Removed custom-build.patch and use Maven rules to ignore
    the unused dependencies
  * Register the Javadoc with doc-base
  * debian/rules: Added a get-orig-source target
  * debian/copyright: Updated the Format URI to 1.0
  * Added debian/orig-tar.sh to build the tarball
    from the upstream SVN repository and documented the process
    in debian/README.source

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 27 Aug 2013 15:30:05 +0200

libgoogle-gson-java (2.1-2) unstable; urgency=low

  * Team upload.
  * Modify d/poms file for m-d-h behavior change. (Closes: #670658)
  * Bump Standards-Version to 3.9.3 (no changes).

 -- tony mancill <tmancill@debian.org>  Fri, 27 Apr 2012 13:55:49 -0700

libgoogle-gson-java (2.1-1) unstable; urgency=low

  * Initial release. (Closes: #654949)

 -- Jakub Adam <jakub.adam@ktknet.cz>  Thu, 05 Jan 2012 23:13:41 +0100
